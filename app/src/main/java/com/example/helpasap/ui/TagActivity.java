package com.example.helpasap.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.example.helpasap.Constant;
import com.example.helpasap.R;
import com.example.helpasap.callback.TagCallback;
import com.example.helpasap.helper.ApiHelper;
import com.example.helpasap.model.TagModel;
import com.loopj.android.image.SmartImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class TagActivity extends Activity implements
        OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    private GridView mGridView;
    private Adapter mAdapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;

    private List<TagModel> mTagList;
    private String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tag);

        initialValue();
        getBundle();
        findViews();
        setListeners();
        loadData();
    }

    private void getBundle() {
        Bundle bundle = getIntent().getExtras();
        tag = bundle.getString(Constant.BUNDLE_TAGS);

        // make the tag to camel case
        String title = tag.substring(0, 1).toUpperCase(Locale.getDefault())
                + tag.substring(1);
        setTitle(title);
    }

    private void initialValue() {
        mTagList = new ArrayList<TagModel>();
    }

    private void findViews() {
        mGridView = (GridView) findViewById(R.id.gridView);

        mAdapter = new Adapter();
        mGridView.setAdapter(mAdapter);

        // set up SwipeRefresh layout
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setColorSchemeColors(getResources().getColor(android.R.color.holo_blue_bright),
                getResources().getColor(android.R.color.holo_green_light),
                getResources().getColor(android.R.color.holo_orange_light),
                getResources().getColor(android.R.color.holo_red_light));
    }

    private void setListeners() {
        mGridView.setOnItemClickListener(this);
        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    private void loadData() {
        mSwipeRefreshLayout.setRefreshing(true);
        ApiHelper.GetTags(this, tag, new TagCallback() {

            @Override
            public void onSuccess(List<TagModel> tagList) {
                super.onSuccess(tagList);

                mTagList.clear();
                mTagList.addAll(tagList);
                mAdapter.notifyDataSetChanged();

                mSwipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFail(String errorMessage) {
                super.onFail(errorMessage);

                mSwipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    @Override
    public void onRefresh() {
        loadData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position,
                            long id) {
        if (parent == mGridView) {
            Intent intent = new Intent(this, PhotoActivity.class);

            String url = Constant.PHOTO_PREFIX + "/"
                    + mTagList.get(position).filename;
            intent.putExtra(Constant.BUNDLE_URL, url);

            // make the tag to camel case
            String title = tag.substring(0, 1).toUpperCase(Locale.getDefault())
                    + tag.substring(1);
            intent.putExtra(Constant.BUNDLE_TAGS, title);

            startActivity(intent);
        }
    }

    class Adapter extends BaseAdapter {
        private LayoutInflater inflater;

        public Adapter() {
            inflater = LayoutInflater.from(TagActivity.this);
        }

        @Override
        public int getCount() {
            return mTagList.size();
        }

        @Override
        public Object getItem(int position) {
            return mTagList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                convertView = inflater.inflate(R.layout.tag_item, null);
                holder = new ViewHolder();
                holder.imageView = (SmartImageView) convertView
                        .findViewById(R.id.image);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String url = "http://sparkslab.info/help/"
                    + mTagList.get(position).filename;
            holder.imageView.setImageUrl(url);

            return convertView;
        }

        class ViewHolder {
            SmartImageView imageView;
        }
    }
}
