package com.example.helpasap.ui;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.helpasap.Constant;
import com.example.helpasap.R;
import com.example.helpasap.helper.ApiHelper;
import com.loopj.android.image.SmartImageView;

public class PhotoActivity extends Activity {
    private SmartImageView mSmartImageView;

    private String url;
    private String tag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        overridePendingTransition(R.anim.friend_in, R.anim.friend_out);
        getBundle();
        findViews();
    }

    private void getBundle() {
        // get url from bundle
        url = getIntent().getStringExtra(Constant.BUNDLE_URL);

        tag = getIntent().getStringExtra(Constant.BUNDLE_TAGS);
        setTitle(tag);
    }

    private void findViews() {
        mSmartImageView = new SmartImageView(this);
        setContentView(mSmartImageView);

        // show the image with the url
        mSmartImageView.setImageUrl(url);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_photo, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.share:
                ApiHelper.shareImage(this, "helpasap.jpg", url, tag, "from Help ASAP");
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.friend_back_in, R.anim.friend_back_out);
    }
}
