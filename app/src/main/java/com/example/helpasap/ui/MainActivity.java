package com.example.helpasap.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.example.helpasap.Constant;
import com.example.helpasap.R;
import com.example.helpasap.callback.ImageProcessCallback;
import com.example.helpasap.helper.PhotoHelper;

import java.io.File;

public class MainActivity extends FragmentActivity implements
        OnClickListener {
    private Button mEarthquakeButton, mElectionsButton, mTaipeiButton,
            mTyphoonButton, mRainButton, mStormButton;
    private File outputPath;

    private static final int REQUEST_CAMERA = 404;
    private static final int REQUEST_GALLERY = 405;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViews();
        setListeners();
    }

    private void findViews() {
        mEarthquakeButton = (Button) findViewById(R.id.earthquake_button);
        mElectionsButton = (Button) findViewById(R.id.elections_button);
        mTaipeiButton = (Button) findViewById(R.id.taipei_button);
        mTyphoonButton = (Button) findViewById(R.id.typhoon_button);
        mRainButton = (Button) findViewById(R.id.rain_button);
        mStormButton = (Button) findViewById(R.id.storm_button);
    }

    private void setListeners() {
        mEarthquakeButton.setOnClickListener(this);
        mElectionsButton.setOnClickListener(this);
        mTaipeiButton.setOnClickListener(this);
        mTyphoonButton.setOnClickListener(this);
        mRainButton.setOnClickListener(this);
        mStormButton.setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_camera:
                // tell camera where to save the photo
                outputPath = PhotoHelper.getUserPhoto(MainActivity.this);

                Intent cameraIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(outputPath));
                startActivityForResult(cameraIntent, REQUEST_CAMERA);
                break;
            case R.id.menu_gallery:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, REQUEST_GALLERY);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (resultCode == Activity.RESULT_OK) {
                    setResult(RESULT_OK);

                    PhotoHelper.resizeCameraImage(this, outputPath,
                            new ImageProcessCallback() {

                                @Override
                                public void onSuccess(String outputPath) {
                                    InputTagDialog dialog = InputTagDialog
                                            .newInstance(outputPath);
                                    dialog.show(getSupportFragmentManager(), "");
                                }
                            });
                }
                break;
            case REQUEST_GALLERY:
                if (resultCode == Activity.RESULT_OK) {
                    setResult(RESULT_OK);

                    PhotoHelper.resizeChosenImage(this, data,
                            new ImageProcessCallback() {

                                @Override
                                public void onSuccess(String outputPath) {
                                    InputTagDialog dialog = InputTagDialog
                                            .newInstance(outputPath);
                                    dialog.show(getSupportFragmentManager(), "");
                                }
                            });
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        String tag = "";
        switch (v.getId()) {
            case R.id.earthquake_button:
                tag = "earthquake";
                break;
            case R.id.elections_button:
                tag = "elections";
                break;
            case R.id.taipei_button:
                tag = "taipei";
                break;
            case R.id.typhoon_button:
                tag = "typhoon";
                break;
            case R.id.rain_button:
                tag = "rain";
                break;
            case R.id.storm_button:
                tag = "storm";
                break;
        }

        // send tag to activity as bundle
        Bundle bundle = new Bundle();
        bundle.putString(Constant.BUNDLE_TAGS, tag);

        Intent intent = new Intent(this, TagActivity.class);
        intent.putExtras(bundle);

        startActivity(intent);
    }
}
