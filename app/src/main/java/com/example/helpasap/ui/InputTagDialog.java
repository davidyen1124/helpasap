package com.example.helpasap.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.helpasap.Constant;
import com.example.helpasap.R;
import com.example.helpasap.UploadPhotoService;

public class InputTagDialog extends DialogFragment {
	private View view;
	private EditText mEditText;

	private String outputPath;

	public static InputTagDialog newInstance(String outputPath) {
		Bundle bundle = new Bundle();
		bundle.putString(Constant.BUNDLE_FILE, outputPath);

		InputTagDialog dialog = new InputTagDialog();
		dialog.setArguments(bundle);

		return dialog;
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		getBundle();

		LayoutInflater inflater = LayoutInflater.from(getActivity());
		view = inflater.inflate(R.layout.input_tag_layout, null);
		findViews();

		return new AlertDialog.Builder(getActivity()).setView(view)
				.setPositiveButton(R.string.upload, new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						Intent intent = new Intent(getActivity(),
								UploadPhotoService.class);
						intent.putExtra(Constant.BUNDLE_FILE, outputPath);
						intent.putExtra(Constant.BUNDLE_TAGS, mEditText
								.getText().toString());
						getActivity().startService(intent);
					}
				}).setNegativeButton(R.string.cancel, null).create();
	}

	private void getBundle() {
		Bundle bundle = getArguments();
		outputPath = bundle.getString(Constant.BUNDLE_FILE);
	}

	private void findViews() {
		ImageView imageView = (ImageView) view.findViewById(R.id.imageView);
		// make a bitmap from outputPath
		Bitmap bmp = BitmapFactory.decodeFile(outputPath);
		// set image using bitmap
		imageView.setImageBitmap(bmp);

		mEditText = (EditText) view.findViewById(R.id.editText);
	}
}
