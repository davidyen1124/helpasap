package com.example.helpasap.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.helpasap.R;

public class LoginActivity extends Activity implements View.OnClickListener {
    private EditText mEditTextUsername, mEditTextPassword;
    private Button mButtonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        findViews();
        setListeners();
        startActivity(new Intent(this, MainActivity.class));
    }

    private void findViews() {
        mEditTextUsername = (EditText) findViewById(R.id.editText_username);
        mEditTextPassword = (EditText) findViewById(R.id.editText_password);
        mButtonLogin = (Button) findViewById(R.id.button_login);
    }

    private void setListeners() {
        mButtonLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.button_login) {
            // if username and password are admin, we can go to MainAcitivty
            if (mEditTextUsername.getText().toString().equals("admin") && mEditTextPassword.getText().toString().equals("admin")) {
                Toast.makeText(this, getString(R.string.welcome_admin), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(this, MainActivity.class));
            } else {
                Toast.makeText(this, "Username or password is invalid", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
