package com.example.helpasap;

import java.io.File;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.example.helpasap.callback.GeneralCallback;
import com.example.helpasap.helper.ApiHelper;

public class UploadPhotoService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		// get picture path from intent
		String picPath = intent.getStringExtra(Constant.BUNDLE_FILE);

		// get tags from intent
		String tags = intent.getStringExtra(Constant.BUNDLE_TAGS);

		// check picture path is not null and has more than one word
		if (picPath == null || picPath.length() <= 0) {
			stopSelf();
			return super.onStartCommand(intent, flags, startId);
		}

		// check picture path is exist and is a file
		final File picFile = new File(picPath);
		if (!picFile.exists() || !picFile.isFile()) {
			stopSelf();
			return super.onStartCommand(intent, flags, startId);
		}

		ApiHelper.Upload(this, picFile, tags, new GeneralCallback() {

			@Override
			public void onSuccess() {
				super.onSuccess();

				// delete the picture file
				picFile.delete();
				stopSelf();
			}

			@Override
			public void onFail(String errorMessage) {
				super.onFail(errorMessage);

				// delete the picture file
				picFile.delete();
				stopSelf();
			}
		});
		return super.onStartCommand(intent, flags, startId);
	}
}
