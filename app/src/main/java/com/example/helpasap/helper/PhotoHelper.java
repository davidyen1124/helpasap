package com.example.helpasap.helper;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.helpasap.R;
import com.example.helpasap.callback.ImageProcessCallback;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class PhotoHelper {

    public static void resizeChosenImage(Context context, final Intent data,
                                         ImageProcessCallback callback) {
        new resizeChosenImageTask(context, data, callback).execute();
    }

    private static class resizeChosenImageTask extends
            AsyncTask<Void, Integer, Void> {

        private Context context;
        private Intent data;
        private ProgressDialog mLoadingDialog;
        private ImageProcessCallback callback;

        public resizeChosenImageTask(Context context, Intent data,
                                     ImageProcessCallback callback) {
            this.context = context;
            this.data = data;
            this.callback = callback;
        }

        @Override
        protected void onPreExecute() {
            mLoadingDialog = ProgressDialog.show(context,
                    context.getString(R.string.please_wait),
                    context.getString(R.string.processing_image), true, false);
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (data != null) {
                Uri selectedImage = data.getData();
                if (selectedImage != null) {
                    Cursor cursor = context.getContentResolver().query(
                            selectedImage, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        // Get file path from column "_data"(1)
                        String photo_path = cursor.getString(1);

                        if (photo_path != null && photo_path.length() > 0) {
                            new resizeImageTask(context, new File(photo_path),
                                    false, false, callback).execute();
                        } else {
                            showError(context, callback,
                                    R.string.unsupported_image_type);
                            return null;
                        }
                    } else {
                        showError(context, callback,
                                R.string.unsupported_image_type);
                        return null;
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                } else {
                    showError(context, callback,
                            R.string.unsupported_image_type);
                }
            } else {
                showError(context, callback, R.string.unsupported_image_type);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
        }
    }

    public static void resizeCameraImage(Context context, File photo_file,
                                         ImageProcessCallback callback) {
        if (photo_file == null || !photo_file.exists()) {
            showError(context, R.string.no_image_found);
            return;
        }
        new resizeImageTask(context, photo_file, true, true, callback)
                .execute();
    }

    private static class resizeImageTask extends AsyncTask<Void, Integer, Void> {

        private Context context;
        private File inputPath, outputPath;
        private boolean showProgressDialog, deleteInputFile;
        private ImageProcessCallback callback;
        private ProgressDialog mLoadingDialog;

        public resizeImageTask(Context context, File inputPath,
                               boolean deleteInputFile, boolean showProgressDialog,
                               ImageProcessCallback callback) {
            this.context = context;
            this.inputPath = inputPath;
            this.deleteInputFile = deleteInputFile;
            this.showProgressDialog = showProgressDialog;
            this.callback = callback;
        }

        @Override
        protected void onPreExecute() {
            if (showProgressDialog) {
                mLoadingDialog = ProgressDialog.show(context,
                        context.getString(R.string.please_wait),
                        context.getString(R.string.processing_image), true,
                        false);
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (inputPath == null || !inputPath.exists()) {
                showError(context, callback, R.string.unsupported_image_type);
                return null;
            }

            final int required_size = 1024;

            BitmapFactory.Options option = new BitmapFactory.Options();
            option.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(inputPath.getAbsolutePath(), option);
            int width = option.outWidth;
            int height = option.outHeight;
            int scale = 1;
            while (true) {
                if (width < required_size || height < required_size)
                    break;
                width /= 2;
                height /= 2;
                scale *= 2;
            }

            option = new BitmapFactory.Options();
            option.inSampleSize = scale;
            Bitmap scaledBitmap = BitmapFactory.decodeFile(
                    inputPath.getAbsolutePath(), option);

            ExifInterface exif;
            try {
                exif = new ExifInterface(inputPath.getAbsolutePath());
            } catch (IOException e) {
                e.printStackTrace();
                showError(context, callback, R.string.image_processing_failed);
                return null;
            }

            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION, 1);
            if (orientation != 1) {
                Matrix matrix = new Matrix();
                switch (orientation) {
                    case 3:
                        matrix.postRotate(180);
                        break;
                    case 6:
                        matrix.postRotate(90);
                        break;
                    case 8:
                        matrix.postRotate(270);
                        break;
                }
                scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0,
                        scaledBitmap.getWidth(), scaledBitmap.getHeight(),
                        matrix, true);
            }

            FileOutputStream out = null;
            outputPath = getUserPhoto(context);
            try {
                out = new FileOutputStream(outputPath);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
                out.flush();
                out.close();
            } catch (Exception e) {
                if (out != null) {
                    try {
                        out.close();
                    } catch (IOException e1) {
                        e1.printStackTrace();
                        showError(context, callback,
                                R.string.saving_photo_failed);
                    }
                }
                e.printStackTrace();
                showError(context, callback, R.string.saving_photo_failed);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            if (showProgressDialog && mLoadingDialog != null) {
                mLoadingDialog.dismiss();
            }
            if (deleteInputFile && inputPath != null && inputPath.exists()) {
                inputPath.delete();
            }
            callback.onSuccess(outputPath.getAbsolutePath());
        }
    }

    // use picture as prefix also add time stamp before .jpg
    public static File getUserPhoto(Context context) {
        return new File(context.getExternalFilesDir(null).getAbsolutePath(),
                String.format("picture%d.jpg", System.currentTimeMillis()));
    }

    private static void showError(final Context context, final int resId) {
        ((Activity) context).runOnUiThread(new Runnable() {

            @Override
            public void run() {
                Toast.makeText(context, resId, Toast.LENGTH_LONG).show();
            }
        });
    }

    private static void showError(final Context context,
                                  final ImageProcessCallback callback, final int resId) {
        ((Activity) context).runOnUiThread(new Runnable() {

            @Override
            public void run() {
                callback.onFailure(context.getString(resId));
            }
        });
    }
}
