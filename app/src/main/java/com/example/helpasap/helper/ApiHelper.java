package com.example.helpasap.helper;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

import com.example.helpasap.R;
import com.example.helpasap.callback.GeneralCallback;
import com.example.helpasap.callback.TagCallback;
import com.example.helpasap.model.TagModel;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class ApiHelper {
	private static AsyncHttpClient mClient = Init();

	private static AsyncHttpClient Init() {
		AsyncHttpClient client = new AsyncHttpClient();
		client.setTimeout(30 * 1000);
		return client;
	}

	private static final String BASE_URL = "http://null.sparkslab.info/course/help";
	private static final String UPLOAD_URL = BASE_URL + "/upload";

	public static void Upload(Context context, File photoFile, String tags,
			final GeneralCallback callback) {
		// check photoPath is valid and is exist
		if (photoFile == null || !photoFile.exists() || !photoFile.isFile()
				|| tags == null || tags.length() <= 0) {
			if (callback != null) {
				callback.onFail("");
			}
			return;
		}

		try {
			RequestParams params = new RequestParams();
			params.put("tags", tags);
			// add photo to RequestParams to upload file
			params.put("picture", photoFile);

			mClient.post(UPLOAD_URL, params, new JsonHttpResponseHandler() {

				@Override
				public void onSuccess(int statusCode, JSONObject response) {
					super.onSuccess(statusCode, response);

					if (callback != null) {
						callback.onSuccess();
					}
				}

				@Override
				public void onFailure(Throwable e, JSONObject errorResponse) {
					super.onFailure(e, errorResponse);

					if (callback != null) {
						callback.onFail("");
					}
				}
			});
		} catch (FileNotFoundException e) {
			e.printStackTrace();

			if (callback != null) {
				callback.onFail("");
			}
			return;
		}
	}

	public static void GetTags(Context context, String tag,
			final TagCallback callback) {
		String url = "";
		try {
			// encode the tag because some words may be weird
			url = BASE_URL + "/" + URLEncoder.encode(tag, "utf-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();

			if (callback != null) {
				callback.onFail("");
			}
		}

		mClient.get(url, new JsonHttpResponseHandler() {

			@Override
			public void onSuccess(int statusCode, JSONArray response) {
				super.onSuccess(statusCode, response);

				List<TagModel> tagList = new ArrayList<TagModel>();

				try {
					for (int i = 0; i < response.length(); i++) {
						JSONObject object = response.getJSONObject(i);

						TagModel model = new TagModel();
						model.filename = object.getString("filename");

						model.tags = new ArrayList<String>();
						JSONArray tagArray = object.getJSONArray("tags");
						for (int j = 0; j < tagArray.length(); j++) {
							model.tags.add(tagArray.getString(j));
						}

						tagList.add(model);
					}

					if (callback != null) {
						callback.onSuccess(tagList);
					}
				} catch (JSONException e) {
					e.printStackTrace();

					if (callback != null) {
						callback.onFail("");
					}
				}
			}

			@Override
			public void onFailure(Throwable error, String content) {
				super.onFailure(error, content);

				if (callback != null) {
					callback.onFail("");
				}
			}
		});
	}

    public static void downloadImage(final Context context, String url,
                                     final File file, final GeneralCallback callback) {
        if (url == null || url.length() == 0 || file == null) {
            callback.onFail(context.getString(R.string.something_error));
            return;
        }

        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, String content) {
                super.onSuccess(statusCode, headers, content);

                try {
                    FileOutputStream stream = new FileOutputStream(file);
                    stream.write(content.getBytes());
                    stream.flush();
                    stream.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                    callback.onFail(context.getString(R.string.download_image_error));
                } catch (IOException e) {
                    e.printStackTrace();
                    callback.onFail(context.getString(R.string.download_image_error));
                } finally {
                    callback.onSuccess();
                }
            }

            @Override
            public void onFailure(Throwable error, String content) {
                super.onFailure(error, content);

                callback.onFail(context.getString(R.string.something_error));
            }
        });
    }


    public static void shareImage(final Context context, String filename,
                                  String url, final String emailSubject, final String emailBody) {
        // save the image in this app's directory
        // after next download this picture should be replaced,
        // so don't worry for using too much storage
        final File file = new File(context.getExternalFilesDir(""), filename);

        final ProgressDialog progressDialog = ProgressDialog.show(context,
                context.getString(R.string.please_wait),
                context.getString(R.string.downloading), true, true, null);

        ApiHelper.downloadImage(context, url, file, new GeneralCallback() {

            @Override
            public void onSuccess() {
                super.onSuccess();

                Intent mEmailIntent = new Intent(
                        android.content.Intent.ACTION_SEND);
                mEmailIntent.setType("image/*");

                mEmailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT,
                        emailSubject);
                mEmailIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                        emailBody);
                mEmailIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));

                context.startActivity(Intent.createChooser(mEmailIntent,
                        context.getString(R.string.share)));

                progressDialog.dismiss();
            }

            @Override
            public void onFail(String message) {
                super.onFail(message);

                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();

                progressDialog.dismiss();
            }
        });
    }
}
