package com.example.helpasap;

public class Constant {
	public static final String PHOTO_PREFIX = "http://sparkslab.info/help";

	public static final String BUNDLE_FILE = "file";
	public static final String BUNDLE_TAGS = "tags";
	public static final String BUNDLE_URL = "url";
}
