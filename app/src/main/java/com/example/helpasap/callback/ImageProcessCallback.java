package com.example.helpasap.callback;

public abstract class ImageProcessCallback {

	public void onSuccess(String filename) {
	}

	public void onFailure(String message) {
	}
}
